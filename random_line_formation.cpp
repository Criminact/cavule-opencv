#include <iostream>
#include <opencv/cv.hpp>

using namespace std;
using namespace cv;

int draw_randomlines(RNG rng,Mat backgroundImage){

    namedWindow("window");

    int Linetype = 8;
    int i = 0;

   for(;;){

    Point pt1 = Point(rng.uniform(0,500),rng.uniform(0,500));
    Point pt2 = Point(rng.uniform(0,500),rng.uniform(0,500));

    line( backgroundImage, pt1, pt2, Scalar(rng.uniform(0,255),rng.uniform(0,255),rng.uniform(0,255)) , rng.uniform(1, 10), 8 );
    imshow("window",backgroundImage);

    if( waitKey(300) >= 0 )
       { return -1; }

   }



}
int main(){

    Mat background_image(500,500,CV_8UC3,Scalar(0,0,0));

RNG rng;

int y = draw_randomlines(rng,background_image);

	return 0;
}


