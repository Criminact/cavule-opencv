#include <iostream>
#include <opencv/cv.hpp>

using namespace std;
using namespace cv;

int main(){

VideoCapture cap(0); // open the default camera
if(!cap.isOpened())  // check if we succeeded
{
cout << "cap was not opened";
return -1;
}	        

    Mat frame,grayscale,split,prev;
    namedWindow("colored");
    namedWindow("grayscale");
    namedWindow("differ");

	cap >> frame;

  
  for(;;)
    {
	frame.copyTo(prev);

        cap >> frame; // get a new frame from camera
	cvtColor(frame, grayscale,COLOR_RGB2GRAY);
	cvtColor(prev, prev,COLOR_RGB2GRAY);
	
	absdiff(prev, grayscale, split);


        imshow("colored", frame);
	imshow("grayscale", grayscale);
	imshow("differ", split);
	

        if(waitKey(60) >= 0) break;
    


}

return 0;
}

