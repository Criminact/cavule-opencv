#include <iostream>
#include <opencv/cv.hpp>

using namespace std;
using namespace cv;

int main(){

Mat image(500,500,CV_8UC3,Scalar(255,255,255));

for(int i=0;i<image.rows/2;i++){
	for(int j=0;j<image.cols/2;j++){

		image.at<Vec3b>(i,j)[0] = 255;
		image.at<Vec3b>(i,j)[1] = 0;
		image.at<Vec3b>(i,j)[2] = 0;

}}


for(int i=image.rows/2;i<image.rows;i++){
	for(int j=0;j<image.cols/2;j++){

		image.at<Vec3b>(i,j)[0] = 0;
		image.at<Vec3b>(i,j)[1] = 255;
		image.at<Vec3b>(i,j)[2] = 0;

}}


for(int i=0;i<image.rows;i++){
	for(int j=image.cols/2;j<image.cols;j++){
		
		image.at<Vec3b>(i,j)[0] = 0;
		image.at<Vec3b>(i,j)[1] = 0;
		image.at<Vec3b>(i,j)[2] = 255;

}}


for(int i=image.rows/2;i<image.rows;i++){
	for(int j=image.cols/2;j<image.cols;j++){
		
		image.at<Vec3b>(i,j)[0] = 255;
		image.at<Vec3b>(i,j)[1] = 255;
		image.at<Vec3b>(i,j)[2] = 255;
}}


namedWindow("whi");
imshow("whi",image);



waitKey(0);

	return 0;
}
